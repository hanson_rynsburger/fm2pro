<?php
function fm2_settings(){
	global $wpdb;

	require_once 'src/facebook.php';

	if (isset($_POST['fm2Auth'])) {
		$id = trim($_POST['apikey']);

		$secret =  trim($_POST['apisecret']);
		$google =  trim($_POST['google']);
		$email =  trim($_POST['email']);
		$fm2timezone =  trim($_POST['fm2timezone']);

		update_post_meta('111111113', 'fm2FbID', $id );
		update_post_meta('111111113', 'fm2FbSecret', $secret );
		update_post_meta('111111113', 'fm2Google', $google );
		update_post_meta('111111113', 'fm2Email', $email );
		update_post_meta('111111113', 'fm2timezone', $fm2timezone );

		$successmessage = " <strong>Settings Saved</strong>";
	}

	$id = get_post_meta(111111113,'fm2FbID', TRUE);
	$secret = get_post_meta(111111113,'fm2FbSecret', TRUE);
	$google = get_post_meta(111111113,'fm2Google', TRUE);
	$email = get_post_meta(111111113,'fm2Email', TRUE);
	$fm2timezone = get_post_meta(111111113,'fm2timezone', TRUE);

	if (!empty($id)) {
		$fb = new Facebook\Facebook([
			'app_id' => $id,
			'app_secret' =>$secret,
		]);

		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['publish_actions,manage_pages,publish_pages']; // optional
		$loginUrl = $helper->getLoginUrl(plugin_dir_url(__FILE__ ).'facebookcallback.php?fbid='.$id.'&secret='.$secret.'' , $permissions);
	}

	$authurl = get_post_meta(111111113,'fm2FbAuthURL', TRUE);
	$authfacebook = plugin_dir_url(__FILE__ ).'authfacebook.php';
	$cron = plugin_dir_url(__FILE__ ).'auto.php';
	$alertcron = plugin_dir_url(__FILE__ ).'auto2.php';
	$cron3 = plugin_dir_url(__FILE__ ).'auto3.php';
	$cron4 = plugin_dir_url(__FILE__ ).'auto4.php';

	echo '<div class = "wrap">
		<div class = "fbvahead">
		'.FANMACHINE2.' </div>
		<h1> Settings</h1>
		<hr />
	';

	echo "<form method='post' action=''>
		<table>
			<tr><td>Facebook App Key: </td><td ><input type='text' name='apikey' value='".$id."'></td></tr>
			<tr><td>Facebook App Secret: </td><td ><input type='text' name='apisecret' value='".$secret."'></td></tr>

			<tr><td>Google API Key</td><td><input type='text' name='google' value='".$google."'  size = '40'>
			<tr><td>Email</td><td><input type='text' name='email' value='".$email."' >";

	echo '
		<tr><td>Select Your Timezone: </td><td>
		<select name="fm2timezone" >';
		for($i= 12; $i>0; $i--){
			if ($fm2timezone == "-".$i){
				echo '<option value= -'.$i.' selected = "selected">UTC -'.$i.':00</option>';
			}
			else{
				echo '<option value= -'.$i.' >UTC -'.$i.':00</option>';
			}
		}
		for($i= 0; $i<13; $i++){
			if ($fm2timezone == "+".$i){
				echo '<option value= +'.$i.' selected = "selected">UTC +'.$i.':00</option>';
			}
			else{
				echo '<option value= +'.$i.' >UTC +'.$i.':00</option>';
			}
		}

	echo "<tr><td>";
	echo "</td><td><input name='fm2Auth' type='submit' value='Save' class = 'button button-primary'></td>";

	if (!empty($loginUrl )) {
		echo "<td><a href = '".$loginUrl."'>Auth Facebook</a></td></tr>";
	}
	else {
		echo "</tr>";
	}

	echo '<tr><td></td><td>'.$successmessage.'</td></tr>
		</table>
	</form>
	<hr />';

	$facebookurl = plugin_dir_url( __FILE__ ) . 'facebookcallback.php';
	echo '<h2>Valid OAuth Redirect URIs</h2>';
    echo "<table>
        <tr><td>Copy following URL to your <code>Valid OAuth Redirect URIs</code> of Facebook APP - Facebook Login panel </td><td colspan = '2'><code>" . $facebookurl . "</code></td></tr>
        <tr></tr>
    </table>";

	echo '<h2>Cron Info</h2>';
	echo "
	<table>
		<tr><td>Once Per Hour Cron URL: </td><td colspan = '2'>wget -O /dev/null ".$cron."</td></tr>
		<tr><td>Once Per Day Cron URL: </td><td colspan = '2'>wget -O /dev/null ".$alertcron."</td></tr>
		<tr><td>Twice Per Hour Cron URL: </td><td colspan = '2'>wget -O /dev/null ".$cron3."</td></tr>
		<tr><td>PRO Cron URL (once per day): </td><td colspan = '2'>wget -O /dev/null ".$cron4."</td></tr>
	</table>";

	echo '<h3>Alternative Cron Commands (Try these if the above dont work on your webhost) </h3>';

	echo "
	<table>
		<tr><td>Once Per Hour URL: </td><td colspan = '2'>lynx -dump /dev/null ".$cron."</td></tr>
		<tr><td>Once Per Day Cron URL: </td><td colspan = '2'>lynx -dump /dev/null ".$alertcron."</td></tr>
		<tr><td>Twice Per Hour  Cron URL: </td><td colspan = '2'>lynx -dump /dev/null ".$cron3."</td></tr>
		<tr><td>PRO Cron URL (once per day): </td><td colspan = '2'>lynx -dump /dev/null ".$cron4."</td></tr>
		<tr></tr>
	</table>";

	echo"</div>";
}
