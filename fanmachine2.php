<?php
/*
Plugin Name:  WP Fan Machine 2 PRO
Plugin URI: http://wpfanmachine.com
Description: Build REAL Fanpage followers on autopilot.
Version: 1.4.6
Author: Dan Green
*/
error_reporting(0);
session_start();
require_once(ABSPATH . 'wp-includes/pluggable.php');
require_once __DIR__ . '/facebook/src/Facebook/autoload.php';

require_once 'fm2install.php';
require_once 'fm2settings.php';
require_once 'fm2campaigns.php';
require_once 'class.fm2campaigns.php';

require_once 'fm2feeds.php';
require_once 'class.fm2feeds.php';

require_once 'fm2pages.php';
require_once 'class.fm2pages.php';

require_once 'fm2comments.php';
require_once 'class.fm2comments.php';

require_once 'fm2commentlog.php';
require_once 'class.fm2commentlog.php';

require_once 'fm2replies.php';
require_once 'class.fm2replies.php';

require_once 'fm2video.php';
require_once 'class.fm2videos.php';
require_once 'fm2YoutubeAPI.php';

require_once 'fm2log.php';
require_once 'class.fm2log.php';

require_once 'fm2growth.php';

require_once 'fm2custom.php';
require_once 'class.fm2custom.php';

require_once 'fm2autoblogging.php';

$image = plugin_dir_url(__FILE__ ).'logo.png';
$favicon = plugin_dir_url(__FILE__ ).'favicon.png';

define("FANMACHINE2", "<img src ='".$image."' />");
define( 'WPSL_URL', plugin_dir_url( __FILE__ ) );

add_action( 'admin_menu', 'fm2_menu' );

register_activation_hook( __FILE__, 'fm2_install' );
add_action( 'admin_enqueue_scripts',  'admin_scripts'  );

require 'plugin_update_check.php';
$MyUpdateChecker = new PluginUpdateChecker_2_0 (
	'https://kernl.us/api/v1/updates/56d1bce5f396ec556bd224fa/',
	__FILE__,
	'fm2pro',
	1
);

add_action( 'wp_dashboard_setup', 'register_fm2pro_dashboard_widget' );
function register_fm2pro_dashboard_widget() {
	wp_add_dashboard_widget(
		'fm2pro_dashboard_widget',
		'Recommended Software For You',
		'fm2pro_promotion_widget_display'
	);
	global $wp_meta_boxes;
	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
	$fm2pro_dashboard_widget = array(
		'fm2pro_dashboard_widget' => $normal_dashboard['fm2pro_dashboard_widget']
	);
	unset( $normal_dashboard['fm2pro_dashboard_widget'] );
	$sorted_dashboard = array_merge( $fm2pro_dashboard_widget, $normal_dashboard );
	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}

function fm2pro_promotion_widget_display() {
	echo file_get_contents('http://fmarketer.me/app-promotions.php');
}

function fm2_menu() {
	$hook = add_menu_page( 'Fan Machine 2', __( 'Fan Machine 2', 'fm2' ),'manage_options', 'fm2', 'fm2_admin', plugin_dir_url(__FILE__ ).'favicon.png' );
	$settings = add_submenu_page( 'fm2', 'Settings', 'Settings', 'manage_options', 'fm2', 'fm2_settings' );
	$campaigns = add_submenu_page( 'fm2', 'Campaigns', 'Campaigns', 'manage_options', 'fm2_campaigns', 'fm2_campaigns' );
	$support = add_submenu_page( 'fm2', 'Support', 'Support', 'manage_options', 'fm2_support', 'fm2_support' );
	// add_action( 'admin_print_styles-' . $campaigns, 'fm2_plugin_admin_styles' );

/*	$search = add_submenu_page( 'fm2', 'Hot Pages', 'Hot Pages', 'manage_options', 'fm2_search_data', 'fm2_search_data' );
	$commenter = add_submenu_page( 'fm2', 'Comments', 'Comments', 'manage_options', 'fm2_commenter', 'fm2_commenter' );
	$replies = add_submenu_page( 'fm2', 'Replies', 'Replies', 'manage_options', 'fm2_replies', 'fm2_replies' );
	$log = add_submenu_page( 'fm2', 'Log', 'Log', 'manage_options', 'fm2_log', 'fm2_log' );(/
	add_action( 'admin_print_styles-' . $page, 'my_plugin_admin_styles' ); */
}

function fm2_support(){
	echo '<div class = "wrap">
		<div class = "fbvahead">
		'.FANMACHINE2.' </div>
		<h1> Support</h1>
		<hr />
		<iframe src="http://wpfanmachine.com/support/fm2support.html" width = "100%" height = "3500px" scrolling = "no"></iframe> ';
}

function fm2_admin(){

}

function admin_scripts() {
	wp_enqueue_style( 'fb-admin-css', plugins_url( 'admin-style.css', __FILE__ ), false );
	wp_enqueue_style('togglescss', plugins_url( 'css/tinytools.toggleswitch.min.css', __FILE__ ), false );
	wp_enqueue_script('jquery');
	wp_enqueue_script('toggles',  plugins_url( 'tinytools.toggleswitch.min.js', __FILE__ ) , false);
	wp_enqueue_script('chart',  plugins_url( 'chart.js', __FILE__ ) , false);
}

function fbva_humanTiming ($time) {
	$time = time() - $time; // to get the time since that moment
	$tokens = array (
		31536000 => 'year',
		2592000 => 'month',
		604800 => 'week',
		86400 => 'day',
		3600 => 'hour',
		60 => 'minute',
		1 => 'second'
	);

	foreach ($tokens as $unit => $text) {
		if ($time < $unit) continue;
		$numberOfUnits = floor($time / $unit);
		return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
	}
}

function wptmFacebookQuery($query, $params) {
	$post_url = 'https://graph.facebook.com/'.$query;
	$accesstoken = get_post_meta(111111113,'fm2accesstoken', TRUE);
	//echo "access token = ".$accesstoken;

	$params = "&limit=5000";
	$post_url = $post_url.'?access_token='.$accesstoken.$params;

	//echo $post_url;

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $post_url);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$result = curl_exec($ch);
	curl_close($ch);

	$result = json_decode($result, TRUE);

	return $result;
}
