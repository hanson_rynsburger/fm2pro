<?php

function fm2PostToBlog($img,$content,$title, $link, $campaign){

	//$content = fm2RemoveImage($content);
	

	$showsource = get_post_meta(111111113,$campaign.'fm2showsource', TRUE);
	$sourcelabel = get_post_meta(111111113,$campaign.'fm2sourcelabel', TRUE);
	$category = get_post_meta(111111113,$campaign.'fm2category', TRUE);	
	$fm2postlength = get_post_meta(111111113,$campaign.'fm2postlength', TRUE);
	$fm2intro = get_post_meta(111111113,$campaign.'fm2intro', TRUE);
	$fm2outro = get_post_meta(111111113,$campaign.'fm2outro', TRUE);
	
	
	if(empty($category)){
		$category = "Uncategorized";
	}
	
	$catid = get_cat_ID( $category );
	
	

	if (!empty($fm2postlength)){
		$content = strip_tags($content, '<br><p><img>');
		$content =  substr ( $content, 0, $fm2postlength );
		$content = $content."... <a href ='".$link."'>Read More</a>";
	}
	
	$spintax = new fm2Spintax();
	$fm2intro= $spintax->process($fm2intro);
	$fm2outro= $spintax->process($fm2outro);
	
	$content= "<p>".$fm2intro."</p><p>".$content;
	
	$content = $content."</p><p>".$fm2outro;
	
	
	
	if($showsource == "yes"){
		$content = $sourcelabel." <a href ='".$link."'>".$link."</a> ".$content."<br />";
	
	}
	
	$fm2draft = get_post_meta(111111113,$campaign.'fm2draft', TRUE);
	if ($fm2draft == "yes"){
		$status = "draft";
	}
	else{
		$status = "publish";
	}
	
	// Create post object
	$my_post = array(
	  'post_title'    => $title,
	  'post_content'  => $content,
	  'post_excerpt' => $content,
	  'post_status'   => $status,
	  'post_author'   => 1,
	'post_category' => array($catid)
	
	);
	global $allowedposttags;
	$allowedposttags['div'] = array('align' => array (), 'class' => array (), 'id' => array (), 'dir' => array (), 'lang' => array(), 'style' => array (), 'xml:lang' => array() );
	$allowedposttags['iframe'] = array('src' => array () );
	
	$post_id = wp_insert_post( $my_post, $wp_error );
	
	return $post_id;
	
	
	
	
	
}

function fm2UploadImage($post_id,$url,$title){  

	require_once(ABSPATH . "wp-admin" . '/includes/image.php');
	require_once(ABSPATH . "wp-admin" . '/includes/file.php');
	require_once(ABSPATH . "wp-admin" . '/includes/media.php');

	
	
	$tmp_name = download_url( $url );
								
	$file_array['name'] = $post_id. '-thumb.jpg';  // new filename based on slug
	$file_array['tmp_name'] = $tmp_name;

	// If error storing temporarily, unlink
	if ( is_wp_error( $tmp_name ) ) {
	@unlink($file_array['tmp_name']);
	$file_array['tmp_name'] = '';
	}

	// do validation and storage .  Make a description based on the Post_ID
	$attachment_id = media_handle_sideload( $file_array, $post_id, 'Thumbnail for ' .$post_id);



	// If error storing permanently, unlink
	if ( is_wp_error($attachment_id) ) {
	$error_string = $attachment_id->get_error_message();
	@unlink($file_array['tmp_name']);
	return;
	}


	// Set as the post attachment
	$post_result= add_post_meta( $post_id, '_thumbnail_id', $attachment_id, true );
	
	$src = wp_get_attachment_url( $attachment_id );
	
	
	return $src; 

//					echo $post_result);
		
}

function fm2RemoveImage($content){

	$content = preg_replace('/<img.+src=[\'"](?P<src>.+?)[\'"].*>/i', '', $content);
	
	return $content;
	

}

?>