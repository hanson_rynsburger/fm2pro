<?php



function fm2_install(){

	global $wpdb;
	$wpdb->show_errors();
	
	

	$table_name = $wpdb->prefix ."fm2pages";   	 



	if($wpdb->get_var("show tables like '$table_name'") != $table_name) { 



		$sql = "CREATE TABLE $table_name (



		id int(11) NOT NULL AUTO_INCREMENT,
		
		node_id text ,

		title text ,
		  
		watching text,
		
		talking_about int(11),
		
		likes int(11),

		keyword text NOT NULL,

		supressed text,
	
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

		dbDelta($sql);    


	}
	
	
	$table_name = $wpdb->prefix ."fm2history"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		ID INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		Post text,		
		Comment text,		
		Timesent int(11),
		Error text,
		campaign text,
		Source text,
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}
	
	$table_name = $wpdb->prefix ."fm2commenthistory"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		ID INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		Post text,		
		Comment text,		
		Timesent int(11),
		Error text,
		campaign text,
		Source text,
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}
	
	

	
	
	
	
	$table_name = $wpdb->prefix ."fm2comments"; 
	
	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {   
	
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		Comment text,	
		campaign text,
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
		
		
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Nice |Great}{|!|post|info|} {|thanks|thanks a lot} {I|} {love|really love} [KEYWORD]',
					'campaign' => 'ALL'				
				
						
		) ); 
		
			$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Interesting|Cool } {post|info|} {|thanks|so much} {this is|} {really great|really good} {more on [KEYWORD] please|} ',
					'campaign' => 'ALL'								
				
						
		) ); 
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Awesome|Tremendous|Amazing} {| post}  {|very informative|very interesting } {{I love | I <3}[KEYWORD]|}',
					'campaign' => 'ALL'			
									
				
						
		) ); 
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'Thanks {|!| for the post| for the info|} {big [KEYWORD] fan here|}',
					'campaign' => 'ALL'							
				
						
		) ); 
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'Who else {loves [KEYWORD]| thinks [KEYWORD] is cool} {?|}',
					'campaign' => 'ALL'							
				
						
		) );
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{who else|who else really} {gets|loves} {this|[KEYWORD]} {?|}',
					'campaign' => 'ALL'							
				
						
		) );
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Absolutely|Always| I always} {love|adore} {everything| anything} {like this|about [KEYWORD]| related to [KEYWORD]}',
					'campaign' => 'ALL'						
				
						
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'{I think | IMO} {posts|stuff|anything} about [KEYWORD] {is|are} {great|fab|fantastic} {who agrees?|}',
					'campaign' => 'ALL'				
				
						
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'anyone {|else} {love| like } {[KEYWORD]|this | this post} as much as {me|i do}  ',
					'campaign' => 'ALL'				
				
						
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'{loving the| love the| such a great}  {post | page | fanpage}  ',								
					'campaign' => 'ALL'			
									
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'{Valuable Post | Important Post| Important Info} {!|} ',								
					'campaign' => 'ALL'			
						
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'Anything { about | related to} {this|[KEYWORD] } is {so|very|really|} important ',								
					'campaign' => 'ALL'			
						
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'More {info|posts|stuff} on [KEYWORD] {please|ok?} {like if you agree|like = agree|who agrees?|} ',								
					'campaign' => 'ALL'							
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'{Worlds|Planets|} {biggest|best} [KEYWORD] {fan|super fan} {|!|right here} ',								
					'campaign' => 'ALL'							
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'{Stuff |posts} {like this| about [KEYWORD]} are why {I love | I like | everyone loves | everyone likes} {social media | facebook | your page | this page} ',								
					'campaign' => 'ALL'							
		) );
		
		$wpdb->insert( $table_name,  array( 						 
					'Comment' =>'{<3| I <3 | Who else? <3| Who else? <3}[KEYWORD] {?|} ',								
					'campaign' => 'ALL'							
		) );
		
		
	}		
	
	$table_name = $wpdb->prefix ."fm2replies"; 
	
	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {   
	
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		Comment text,	
		campaign text,
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
		
		
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Agreed|Agreed...|I agree|Yup|Yeah|+1|<3|} {Amazing|Nice|Sweet|Awesome|Cool} {|!|comment|info|post|reply} {|thanks|thanks a lot|thanks so much|?}',
					'campaign' => 'ALL'		
									
				
						
		) ); 
		
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Agreed|Agreed...|I agree|Yes!|Yes indeed|Anyone else agree?|I Agree anyone else?|thanks for that!|much needed comment!}',
					'campaign' => 'ALL'		
									
				
						
		) ); 
		
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Nice|Agreed|Yup|+1|Yeah|<3|<3 This} ',
					'campaign' => 'ALL'		
									
				
						
		) ); 
		
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Absolutely Agree|Much Agreed|Yup Indeed|+1 this|Yeah Agreed|} ',
					'campaign' => 'ALL'		
									
				
						
		) ); 
		
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{Awesome|Thanks|Thanks so much for that|Yeah|}{<3|:)|:D|} ',
					'campaign' => 'ALL'		
									
				
						
		) ); 
	
		$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{yup|yep|Yes!|Yeah!|Hell yeah!|heck yes|yes yes!}{<3|:)|:D|} ',
					'campaign' => 'ALL'		
									
				
						
		) ); 
		
			$wpdb->insert( $table_name,  array( 
						 
					'Comment' =>'{totally|absolutely} {right|correct|} {{couldnt | could not} agree more|} {:)|:D|} ',
					'campaign' => 'ALL'		
									
				
						
		) ); 
		
	}

	$table_name = $wpdb->prefix ."fm2Campaigns"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		name text NOT NULL,
		status text NOT NULL,
		
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}

	$table_name = $wpdb->prefix ."fm2likesgrowth"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		campaign text,
		time int(11),
		likes int(11),
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}
	
		$table_name = $wpdb->prefix ."fm2Videos"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		type text,
		url text,	
		hashtags text,	
		campign INTEGER(100),
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}
	
	
	$table_name = $wpdb->prefix ."fm2PostHistory"; 
   
	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		ID INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		Source text,		
		Message text,		
		Timesent int(11),
		Status text,
		Campaign int(11),
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}
	
	$table_name = $wpdb->prefix ."fm2Images"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		username text,	
		hashtags text,		
		campaign INTEGER(100),
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}
	
	$table_name = $wpdb->prefix ."fm2Feeds"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		url text,	
		hashtags text,		
		campaign INTEGER(100),
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}
	
	$table_name = $wpdb->prefix ."fm2growth"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		campaign text,
		time int(11),
		likes int(11),
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}
	
	
	$table_name = $wpdb->prefix ."fm2Custom"; 

	if($wpdb->get_var("show tables like '$table_name'") != $table_name) {
  
		$sql = "CREATE TABLE $table_name (
		id INTEGER(100) UNSIGNED AUTO_INCREMENT,   
		message text,	
		url text,	
		campaign INTEGER(100),
		UNIQUE KEY id (id)
		 )ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci AUTO_INCREMENT=1;";

		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);    
	}




}


