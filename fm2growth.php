<?php

function fm2_growth($campaign){

	global $wpdb;
	
	$table_name = $wpdb->prefix ."fm2growth"; 
	
	
	$query = "SELECT likes FROM $table_name WHERE campaign ='$campaign' ORDER BY time desc ";
	
	$results = $wpdb->get_results($query);
	
	$i = 0;
	$data = array();
	foreach ($results as $result){
		if ($i == 7){
			 break;
		}
		$data[$i] = $result->likes;
		$i++;
		
	}
	
	for ($i = 0; $i<7; $i++){
		if (empty($data[$i])){
			$data[$i] = 0;
		}
	
	}
	
	echo'
		<div class = "graphcontainer">	
			<div class = "graph"  id = "graphcontainer">			
				<canvas id="canvas" height="400" width="600" class = "graph"></canvas>			
			</div>
	
			
		</div>
		
		
		<script>
					var lineChartData = {
						labels : ["7 Days","6 Days","5 Days","4 Days","3 Days","2 Days","Today"],
						 datasets : [
							{
							    fillColor : "rgba(220,220,220,0.5)",
							    strokeColor : "rgba(220,220,220,1)",
							    pointColor : "rgba(220,220,220,1)",
							    pointStrokeColor : "#fff",
							   data : ['.$data[6].','.$data[5].','.$data[4].','.$data[3].','.$data[2].','.$data[1].','.$data[0].']
							}
							
						]
					}				
				
					var ctx = document.getElementById("canvas").getContext("2d");
					window.myLine = new Chart(ctx).Line(lineChartData, {
						 responsive : false,
						   animation: true,
						   barValueSpacing : 5,
						   barDatasetSpacing : 1,
						   tooltipFillColor: "rgba(0,0,0,0.8)",                
						   multiTooltipTemplate: "<%= datasetLabel %> - <%= value %>"
						
					});

		</script>	';					
		
	//data : [0,56,120,201,250,326,459]
     //  data : ['.$data[6].','.$data[5].','.$data[4].','.$data[3].','.$data[2].','.$data[1].','.$data[0].']
   
   echo "<div>";
   
   echo '<canvas id="myChart" width="400" height="400">test canvas</canvas>';

   
   
   echo "</div>";
   
  
   
}
