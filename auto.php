<?php

require('../../../wp-blog-header.php');

set_time_limit(200);
function return_300( $seconds )
{
  // change the default feed cache recreation period to 2 hours
  return 300;
}


add_filter( 'wp_feed_cache_transient_lifetime' , 'return_300' );




$campaign = fm2SelectCampaign();



fm2Postvideo($campaign);
fm2PostFeed($campaign);

fm2PostCustom($campaign);
	
	
	


function fm2PostCustom($campaign){
			
	$frequency =  get_post_meta(111111113,$campaign.'fm2CustomPeriod', TRUE);	
	
	if (fm2ChooseToPost($frequency)){
	
		

		global $wpdb;    
		$wpdb->show_errors();
		
		$table_name = $wpdb->prefix ."fm2Custom"; 

		$query = "SELECT * FROM $table_name WHERE campaign = $campaign  ORDER BY RAND()"; 
		$querydata = $wpdb->get_results($query);	
		
		foreach ( $querydata as $row ) {	
				$comment = $row->message;
				$link = $row->url;	
		}	
	
	
		if (!empty($comment)){
		
			$spintax = new fm2Spintax();
			$comment= $spintax->process($comment);
			fm2PostToFacebook('Custom', $link, $comment,'', $campaign, $hashtag);
		}		
	}		
}

function fm2SelectCampaign(){
	global $wpdb;    
	$wpdb->show_errors();
	
	$table_name = $wpdb->prefix ."fm2Campaigns"; 

	$query = "SELECT id FROM $table_name WHERE  name != '' AND status = 'ON' ORDER BY RAND()"; 
	$campaign  = $wpdb->get_var($query );
	return $campaign;
	
}

function fm2PostFeed($campaign){
	$fm2feedpost = get_post_meta(111111113,$campaign.'fm2feedpost', TRUE);
	$frequency =  get_post_meta(111111113,$campaign.'fm2FeedPeriod', TRUE);	
	$fm2draft = get_post_meta(111111113,$campaign.'fm2draft', TRUE);
	
	if (fm2ChooseToPost($frequency)){
		
		global $wpdb;    
		$wpdb->show_errors();
		
		$table_name = $wpdb->prefix ."fm2Feeds"; 

		$query = "SELECT * FROM $table_name WHERE campaign = $campaign ORDER BY RAND()"; 
		$querydata = $wpdb->get_results($query);

		foreach ( $querydata as $row ) {	
			$url = $row->url;	
			if (!empty($url)){
				$hashtags = $row->hashtags;			
				$hashtags = explode(',', $hashtags);			
				$hashtag = $hashtags[array_rand($hashtags)];			
				
				$rss = fetch_feed( $url );

				
				if (!is_wp_error( $rss ) ){ // Checks that the object is created correctly
					$maxitems = $rss->get_item_quantity(0);		
					
					  // Build an array of all the items, starting with element 0 (first element).
					$rss_items = $rss->get_items( 0, $maxitems );
					
					foreach ( $rss_items as $item ){	 
						$link = $item->get_permalink();
						$content = $item->get_content();		
						$title = $item->get_title();
						$imgURL = fm2GetImgSrc($content);
						$inhistory = fm2CheckHistory($title,$campaign);
						if ($inhistory == 0){	
							if  ($fm2feedpost == "yes"){	
								$postid = fm2PostToBlog($imgURL,$content,$title, $link,$campaign);
								if ($fm2draft == "yes"){
									fm2PostToFacebook('Feed', $link, $title,$imgURL, $campaign, $hashtag);
								}
								else{								
									$permalink = get_permalink($postid);
									fm2PostToFacebook('Feed', $permalink, $title,$imgURL, $campaign, $hashtag);
								}	
							}
							else{
								fm2PostToFacebook('Feed', $link, $title,$imgURL, $campaign, $hashtag);
							}
							return;
						}		
					}			
				}
			}
			else{
				$error = $rss->get_error_message();
				
			}			
		 }
	} 
		 
}

function fm2GetImgSrc($content){

	$matches = array();
	
	$match = preg_match('/\bhttps?:\/\/\S+(?:png|jpg)\b/' , $content , $matches);
	
	if ($match == 1){
		return $matches[0];
	}	
	else{
		return "";
	}

}






function fm2Postvideo($campaign){

	
	$fm2videopost = get_post_meta(111111113,$campaign.'fm2videopost', TRUE);	
	$frequency = get_post_meta(111111113,$campaign.'fm2VideoPeriod', TRUE);	
	$fm2draft = get_post_meta(111111113,$campaign.'fm2draft', TRUE);
	
	if (fm2ChooseToPost($frequency)){	
			
		global $wpdb;    
		$wpdb->show_errors();
		
		$google = get_post_meta(111111113,'fm2Google', TRUE);
		$youtube = new fm2Youtube(array('key' => $google));
		
		if (empty($google)){
			return;
		}
		
		$table_name = $wpdb->prefix ."fm2Videos"; 

		$query = "SELECT * FROM $table_name WHERE campign = $campaign ORDER BY RAND()"; 
		$querydata = $wpdb->get_results($query);

		foreach ( $querydata as $row ) {
			$url = $row->url;
			$type = $row->type;
			$date = $row->datefrom;
			
				
			
			
			
			if ($type == 'channel'){
				if (!empty($url)){	
					$hashtags = $row->hashtags;			
					$hashtags = explode(',', $hashtags);			
					$hashtag = $hashtags[array_rand($hashtags)];		

					
					
					$channel = $youtube->getChannelFromURL($url);
					$id = $channel->id;
					$datetime= date("c", strtotime("$date"));
					$response = $youtube->searchChannelVideos('', $id, $datetime);		
					
					foreach ($response as $video){
					
						$thumbnail = $video->snippet->thumbnails->default->url; 
						$title = $video->snippet->title;
						$description =  $video->snippet->description;		
						$id = $video->id->videoId;	
						$link = 'http://youtu.be/'.$id;	
						$inhistory = fm2CheckHistory($title,$campaign);
						
						
						if ($inhistory == 0){							
							if  ($fm2videopost == "yes"){	
								$embedcode ='<iframe width="640" height="360" src="//www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';
								$postid = fm2PostToBlog($thumbnail,$embedcode,$title, $link,$campaign);
								$permalink = get_permalink($postid);
								if ($fm2draft == "yes"){
									fm2PostToFacebook('Video', $link, $title,'', $campaign, $hashtag);
								}
								else{			
								
									fm2PostToFacebook('Video', $permalink, $title, '', $campaign, $hashtag);
								}	
							}
							else{							
								fm2PostToFacebook('Video', $link, $title, '', $campaign, $hashtag);
							
							}
							return;
						}
							
						
					}	
				
				}
					
			}
			else if ($type == 'playlist'){			
				if (!empty($url)){		
					$playlist = $youtube->getPlaylistFromURL($url);
					$response =  $youtube->getPlaylistItemsByPlaylistId($playlist);	
					
					$hashtags = $row->hashtags;			
					$hashtags = explode(',', $hashtags);			
					$hashtag = $hashtags[array_rand($hashtags)];			
					

					foreach ($response as $video){				
						$id = $video->contentDetails->videoId;	
						$title = $video->snippet->title;	
						$link = 'http://youtu.be/'.$id;	
						
						
						$inhistory = fm2CheckHistory($title,$campaign);
					
						if ($inhistory == 0){		
							
							if  ($fm2videopost == "yes"){	
								$embedcode ='<iframe width="640" height="360" src="//www.youtube.com/embed/'.$id.'" frameborder="0" allowfullscreen></iframe>';
								$postid = fm2PostToBlog($thumbnail,$embedcode,$title, $link,$campaign);
								$permalink = get_permalink($postid);
								fm2PostToFacebook('Video', $link, $title, '', $campaign, $hashtag, "");
							}
							else{							
								fm2PostToFacebook('Video', $link, $title, '', $campaign, $hashtag, "");
							
							}
							return;
						}	
					}
				}
			}
			
			
			
		}		
	}
}





function fm2PostToFacebook($source, $link, $title, $imgURL, $campaign, $hashtag){	
		
		
		$title =  html_entity_decode($title);
		
		$accesstoken = get_post_meta(111111113,$campaign.'pageaccesstoken', TRUE);	
		$fbpageid = get_post_meta(111111113,$campaign.'fbpageid', TRUE);
		//echo "accesstoken = ".$accesstoken;
		
	
		$data['link'] = $link;
		$data['message'] = $title;
		$data['access_token'] = $accesstoken;
		
		$post_url = 'https://graph.facebook.com/'.$fbpageid.'/feed';
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $post_url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$return = curl_exec($ch);
		curl_close($ch);
		
		$return = json_decode($return);
	
		
		fm2InsertHistory($source, $code, $link, $title, $campaign);	
	
	
	
}





function fm2ChooseToPost($frequency){

	if ($frequency == 1){
		$frequency = 0;
	}
	$random = mt_rand(1,40);

	$powered = pow($frequency, 2);	
	
	//echo "<p>".$frequency. " ".$random. " ".$powered;

	
	if($random <= $powered+1){	
		
		return 1;
	}
	else{
		return 0;
	}

}

function fm2InsertHistory($source, $code, $link, $title, $campaign){

	global $wpdb;    
	$wpdb->show_errors();
	$table_name = $wpdb->prefix ."fm2history"; 
	

	
	
	
	$result = $wpdb->insert( $table_name,  array( 
		'Source' =>$source,			
		'Comment' =>$title,		
		'Timesent'=>time(),
		'Error'=>$code,
		'Campaign' =>$campaign
	) ); 	

	

}

function fm2CheckHistory( $title, $campaign){
	global $wpdb;    
	$wpdb->show_errors();	
	
	
	
	$title = html_entity_decode($title);
	$table_name = $wpdb->prefix ."fm2history"; 
	
	
	$title = addslashes($title);
	
	
	$query = "SELECT * FROM $table_name WHERE Comment = '$title' AND Campaign = $campaign ";    
	
	
	
	$result  = $wpdb->get_row($query);
	
		
	

	
	if ($result == null){
		
		return 0;	
		
	}
	else{
		
		return 1;
	
	}

}

class fm2Spintax
{
    public function process($text)
    {
        return preg_replace_callback(
            '/\{(((?>[^\{\}]+)|(?R))*)\}/x',
            array($this, 'replace'),
            $text
        );
    }

    public function replace($text)
    {
        $text = $this->process($text[1]);
        $parts = explode('|', $text);
        return $parts[array_rand($parts)];
    }
}




?>